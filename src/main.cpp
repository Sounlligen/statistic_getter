#include <string>
#include <chrono>
#include <ctime>
#include <vector>
#include <cmath>

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/BatteryState.h>

#include "CSV.hpp"

double frequency;

double distance;
double linear_vel;
double linear_acc;
double angular_vel;
double angular_acc;
double battery_state;
nav_msgs::Odometry prev_odom;


void odometryCallback(const nav_msgs::OdometryConstPtr &odom)
{
    distance += sqrt(pow(odom->pose.pose.position.x - prev_odom.pose.pose.position.x, 2) +
                     pow(odom->pose.pose.position.y - prev_odom.pose.pose.position.y, 2));

    linear_vel = odom->twist.twist.linear.x;
    angular_vel = odom->twist.twist.angular.z;

    linear_acc = (odom->twist.twist.linear.x - prev_odom.twist.twist.linear.x)*frequency;
    angular_acc = (odom->twist.twist.angular.z - prev_odom.twist.twist.angular.z)*frequency;

    prev_odom.pose.pose.position.x = odom->pose.pose.position.x;
    prev_odom.pose.pose.position.y = odom->pose.pose.position.y;

    prev_odom.twist.twist.linear.x = odom->twist.twist.linear.x;
    prev_odom.twist.twist.angular.z = odom->twist.twist.angular.z;
}

void batteryCallback(const sensor_msgs::BatteryStateConstPtr &battery)
{
    battery_state = battery->percentage;
}

int main (int argc, char **argv)
{
    std::string odom_topic;
    std::string battery_topic;
    std::string filepath;
    std::vector<std::string> headers = {
        "Time from start (s)",
        "Distance covered (m)",
        "Linear velocity (m/s)",
        "Linear acceleration (m/s^2)",
        "Angular velocity (rad/s)",
        "Angular acceleration (m/s^2)",
        "Battery level (%)"
    };

    std::chrono::time_point<std::chrono::system_clock> system_time = std::chrono::system_clock::now();
    std::time_t system_current_time = std::chrono::system_clock::to_time_t(system_time);

    ros::init(argc, argv, "statistic_getter");

    ros::NodeHandle n;

    n.getParam("/statistic_getter/file_path", filepath);
    n.getParam("/statistic_getter/odom_topic", odom_topic);
    n.getParam("/statistic_getter/battery_topic", battery_topic);
    n.getParam("/statistic_getter/frequency", frequency);

    ros::Rate rate(10);

    ros::Subscriber odom_sub = n.subscribe<nav_msgs::Odometry>(odom_topic.c_str(), 1, &odometryCallback);
    ros::Subscriber batter_sub = n.subscribe<sensor_msgs::BatteryState>(battery_topic.c_str(), 1, &batteryCallback);

    if(filepath.back() != '/')
    {
        filepath += "/";
    }

    filepath += std::ctime(&system_current_time);
    filepath += ".csv";

    jay::util::CSVwrite csv_write(filepath);

    if(csv_write.error)
    {
        ROS_INFO("%s", csv_write.error_msg.c_str());
        exit(1);
    }
    else
    {
        ROS_INFO("File opened!");
    }

    csv_write.WriteRecord(headers);

    distance = 0;
    linear_vel = 0;
    linear_acc = 0;
    angular_vel = 0;
    angular_acc = 0;
    battery_state = 0;

    prev_odom.pose.pose.position.x = 0;
    prev_odom.pose.pose.position.y = 0;
    prev_odom.twist.twist.linear.x = 0;
    prev_odom.twist.twist.angular.z = 0;

    ros::Time ros_start_time = ros::Time::now();

    while(ros::ok())
    {
        ros::Duration ros_time_passed = ros::Time::now() - ros_start_time;

        csv_write.WriteField(std::to_string(ros_time_passed.toSec()));
        csv_write.WriteField(std::to_string(distance));
        csv_write.WriteField(std::to_string(linear_vel));
        csv_write.WriteField(std::to_string(linear_acc));
        csv_write.WriteField(std::to_string(angular_vel));
        csv_write.WriteField(std::to_string(angular_acc));
        csv_write.WriteField(std::to_string(battery_state), true);

        rate.sleep();
        ros::spinOnce();
    }
}
